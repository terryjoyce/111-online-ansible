#!/bin/sh

# exit immediately on error
set -e

# make SSH config
mkdir -p /root/.ssh
cp ssh/* /root/.ssh/
chmod -R 0700 /root/.ssh/

# we have to ensure the vault password file ISN'T executable
# and that it lives at /tmp/ansible_vaultpass.txt

# if vault password file is mounted then use the contents of that as the password
if [ -f vaultpass.txt ] ; then
	VAULTPASS=$(cat vaultpass.txt)
fi

# save vault password to file
if [ -z "$VAULTPASS" ] ; then
	echo -n "Enter the vault password and press [ENTER]: "
	read -s VAULTPASS
fi

if [ "$VAULTPASS" == "" ] ; then
	echo "VAULTPASS not defined, ansible will probably fail, if that's what you're trying to run"
else
	echo "$VAULTPASS" > /tmp/ansible_vaultpass.txt
	chmod 600 /tmp/ansible_vaultpass.txt
fi




exec "$@"
