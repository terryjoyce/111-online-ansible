FROM ubuntu:16.04

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y git libssl-dev python-pip build-essential libssl-dev libffi-dev python-dev dos2unix

WORKDIR /ansible

RUN pip install --upgrade pip

COPY pip_install.txt .
RUN pip install -r pip_install.txt

COPY ansible_roles.yml .
RUN ansible-galaxy install -r ansible_roles.yml


ENTRYPOINT ["bash", "/ansible/entrypoint.sh"]
CMD ansible-playbook

COPY . /ansible
