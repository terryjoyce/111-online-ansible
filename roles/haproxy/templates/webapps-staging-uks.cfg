global
    log-send-hostname
    log     /dev/log    local0
    tune.ssl.default-dh-param   2048

defaults
    mode        http
    timeout     client  1m   # maximum inactivity time on the client side
    timeout     server  1m   # maximum inactivity time on the server side
    timeout     connect 40s  # maximum time to wait for a connection attempt to a server to succeed
    timeout     check   40s  # maximum time to wait for a check attempt
    retries     5
    option      redispatch
    maxconn     4000
    errorfile 500 /etc/haproxy/errors/500.http
    errorfile 503 /etc/haproxy/errors/500.http

# STATS LISTENER
listen http_stats
    bind    :8181
    log     global
    stats   enable
    stats   uri /
    stats   show-legends
    stats   show-node
    stats   refresh     5s
    stats   admin if TRUE

#FRONTEND HTTP
listen http
    bind :80
    log global
    mode http

    log-format "[%t] c_ip=%ci c_port=%cp frontend=%ft backend=%b server=%s tq=%Tq tw=%Tw tc=%Tc tr=%Tr tt=%Tt status=%ST bytes=%B req_cookies=%CC res_cookies=%CS term_state=%tsc actconn=%ac feconn=%fc beconn=%bc srv_conn=%sc retries=%rc srv_queue=%sq backend_queue=%bq req_hdr=%hr res_hdr=%hs http_request=%{+Q}r"

    option forwardfor header X-Client-IP

    capture request header  host              len 50
    capture request header  True-Client-IP    len 50
    capture request header  User-Agent        len 512
    capture request header  Referer           len 2048

    capture response header Location          len 200
    capture response header Cache-Control     len 50

    redirect scheme https

#FRONTEND HTTPS
frontend https
    bind :443 ssl crt /etc/haproxy/default.pem no-sslv3
    log global
    mode http

    log-format "[%t] c_ip=%ci c_port=%cp frontend=%ft backend=%b server=%s tq=%Tq tw=%Tw tc=%Tc tr=%Tr tt=%Tt status=%ST bytes=%B req_cookies=%CC res_cookies=%CS term_state=%tsc actconn=%ac feconn=%fc beconn=%bc srv_conn=%sc retries=%rc srv_queue=%sq backend_queue=%bq req_hdr=%hr res_hdr=%hs http_request=%{+Q}r"

    capture request header  host              len 50
    capture request header  True-Client-IP    len 50
    capture request header  User-Agent        len 512
    capture request header  Referer           len 2048

    capture response header Location          len 200
    capture response header Cache-Control     len 50

    option forwardfor header X-Client-IP
    http-request set-header X-Forwarded-Port %[dst_port]

    reqadd X-Forwarded-Proto:\ https

    rspidel (Server|X-AspNet-Version|X-AspNetMvc-Version|X-Powered-By)

#Robots.txt
    acl is_robots path /robots.txt
    use_backend robots if is_robots

    acl DISABLEFLAG srv_is_up(check_disable_webapp/CHECK)
    use_backend disable_webapp if DISABLEFLAG

    acl 111NHSUK hdr(host) -i staging.111.nhs.uk
    acl 111NHSUK hdr(host) -i test.staging.111.nhs.uk

    acl 111NHSUK-dead nbsrv(web-backend) lt 1
    use_backend web-backend-backup if 111NHSUK 111NHSUK-dead
    use_backend web-backend if 111NHSUK

    acl PATHWAYS hdr_reg(host) -i uk[ws]-s-pathwaysearch(-ext)?\.staging\.111\.nhs\.uk
    use_backend pathwaysearch-backend if PATHWAYS

# BACKENDS

#Robots txt
backend robots
    errorfile 503 /etc/haproxy/errors/robots.http

backend web-backend
    errorfile 500 /etc/haproxy/errors/500.http
    errorfile 502 /etc/haproxy/errors/500.http
    errorfile 503 /etc/haproxy/errors/500.http
    http-response set-header Strict-Transport-Security max-age=15552000;
    option httpchk GET / HTTP/1.1\r\nHost:\ 111-uks-s-web.azurewebsites.net/?Campaign=NHS111Testing\r\nAuthorization:\ Basic\ MTExbGl2ZTpBVGVhbQ==
    http-check expect ! status 403

    reqirep ^Host:\ .* Host:\ 111-uks-s-web.azurewebsites.net
    server webapp-web 111-uks-s-web.azurewebsites.net:443 check ssl verify none inter 5s

backend web-backend-backup
    errorfile 403 /etc/haproxy/errors/500.http
    http-response set-header Strict-Transport-Security max-age=15552000;
    option httpchk GET / HTTP/1.1\r\nHost:\ 111-ukw-s-web.azurewebsites.net/?Campaign=NHS111Testing\r\nAuthorization:\ Basic\ MTExbGl2ZTpBVGVhbQ==
    http-check expect ! status 403

    reqirep ^Host:\ .* Host:\ 111-ukw-s-web.azurewebsites.net
    server webapp-web-backup 111-ukw-s-web.azurewebsites.net:443 check ssl verify none inter 5s

backend disable_webapp
    errorfile 503 /etc/haproxy/errors/disable.http

backend check_disable_webapp
    option httpchk GET /flags/staging-disabled.txt HTTP/1.1\r\nHost:\ 111ukstorage.blob.core.windows.net\r\nUser-agent:\ HAProxyProbe
    server CHECK 111ukstorage.blob.core.windows.net:443 check ssl verify none

backend pathwaysearch-backend
    option httpchk GET /pathways/_stats
    http-check expect status 200

    server uks-s-ldk01-9200 111-uks-s-ldk01-int.uks.111.nhs.uk:9200 check weight 100
    server uks-s-ldk02-9200 111-uks-s-ldk02-int.uks.111.nhs.uk:9200 check weight 100
    server backupCheck ukw-s-pathwaysearch-ext.staging.111.nhs.uk:443 ssl verify none backup
