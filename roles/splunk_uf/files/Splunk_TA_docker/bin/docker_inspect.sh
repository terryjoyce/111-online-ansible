#!/bin/bash

DOCKER_BIN=/usr/bin/docker
"$DOCKER_BIN" inspect $("$DOCKER_BIN" ps -aq) | jq -c -M -r ".[]"

