storage "azure" {
  accountName = "{{ vault_accountname }}"
  accountKey  = "{{ vault_accountkey }}"
  container   = "{{ vault_container }}"
}

listener "tcp" {
  address     = "0.0.0.0:8200"
  tls_disable = true
}

ui = true
