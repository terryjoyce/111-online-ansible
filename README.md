# 111 Infra Ansible Config

THIS IS A DIRECT COPY FROM THE EXISTING REPO AND NEEDS UPDATING

This will allow you deploy the roles to CentOS 7 and Ubuntu 16.04

So far the setup has been to do a basic install with no configuration of

- Docker
  - TAG=docker
- Fail2Ban
  - TAG=fail2ban
- NRPE
  - TAG=nrpe
- Splunk Universal Forwarder (inc. sysstat)
  - TAG=splunkuf
- and Updating of the host OS
  -TAG=update

## Required secret information!

Before you can run any Ansible playbooks on remote hosts, you'll need the following information:

- The Ansible vault password:
  - Used to decrypt any files that are encrypted, normally under /group_vars/ and normally have the file name `vault.yml`.
- The SSH private key used by ansible:
  - Used by ansible to connect to hosts via SSH.
  - the associated public key is deployed to the `choicesadmin` users `~/.ssh/authorized_keys` file.
  - Allows password-less access to all the hosts.

### Using the vault password

This can be used in a number of ways, there are (in order of precedence):

1. Create a `vaultpass.txt` file in your working ansible repo
   then as part of the `docker run` step add the file as a volume:
   ` -v /c/Users/user/Documents/GitRepos/111-ansible/vaultpass.txt:/ansible/vaultpass.txt`
   When the container starts the password will get read from that file.
2. Pass the password in the docker run command as an environment variable, like:
   ` docker run -e VAULTPASS=<vault password> ... `
3. Don't do anything! When the container starts you'll be prompted for a password.
   NOTE: This requires the `-it ` parameter to be added to the command line, otherwise it'll just escape straight out and exit.

If an invalid password or no password is provided you'll get encryption failed messages.

### SSH Key configuration

Ansible uses SSH to connect to all of the hosts it manages it. SSH can be set up 1 of 2 ways:

1. Traditional password request/response.
2. Private/Public key.

As the SSH port that we connect to is available publically over the internet, we use SSH keys.

These have already been provisioned as part of the initial configuration for this environment, but the way we do this is as follows:

1. Create a SSH key pair using either a Linux box or "git for bash"
    1. Run `ssh-keygen -b 4096 -C 111-ansible -f ./111-ansible`
       This will ask for a passphrase, just leave this blank.
2. This will create a private key called `111-ansible` and a public key called `111-ansible.pub`
3. The public key can be inserted into the provisioning scripts, or users `authorized_keys` file. Examples include:
    1. When creating a Linux box in the Azure web interface.
    2. Inserted into either the `provision_staging.yml` or `provision_production.yml` files.
    3. In the `add_ansible_user.sh` script.
4. The private key should get stored securely in PasswordSafe and also in the `ssh/` folder in your working directory, with the name `id_rsa`.

When ansible runs it attempts to copy the `/ansible/ssh/id_rsa` file to `/root/.ssh/id_rsa`. This is so that the root user can connect to the provisioned servers using that private key.

### Jump/Bastion Host

For security reasons we don't open SSH ports public to all the hosts. This means that the machine that ansible is running on probably can't connect to them.

To get around this, we use a "Jump" host. This is basically a double hop, we can only connect to `111-s-jump01`, and then from there we can connect to all the others.

This is configured in the `group_vars/azure_machines.yml` variable file.

## Running playbooks

Now you're in a position to run playbooks! The recommended way to run all playbooks to ensure consistency is using docker.

Running it inside docker means it will run the same way whether you're on Linux, Windows or OS X.

Before you can do anything else, the first thing you need to do is build a docker image. This is done by running:

`docker build -t 111-ansible .`

This creates a docker "image" which you can run later on. What this command does is:

1. Use the `Dockerfile` to create a new image.
2. This uses Ubuntu as the base.
3. It installs git/python/pip, everything required to build the ansible package.
4. Uses pip to install packages from `pip_install.txt`, this includes ansible (set to a specific version)
5. Runs `ansible-galaxy` with the file `ansible_roles.txt`, which install external roles we use.
6. Sets the script (`entrypoint.sh`) which runs on every `docker run` before user defined commands.
7. Copies the current working directory to the `/ansible` directory, excluding items from `.dockerignore`.

This image can then be run now using `docker run`. You can run it now by running `docker run -it --rm 111-ansible` but this will fail as it's missing a command to run, and, more importantly, a SSH key volume.

The first thing is to mount the SSH folder into the docker command. We don't build the image with the key baked into it as it's a security risk, so we mount it with the -v command:

`docker run -it --rm -v /c/Users/user..../ssh/:/ansible/ssh/ 111-ansible ls ssh/`

This command will put *MY* ssh folder in `/ansible/ssh`. When the docker container starts, it runs the `entrypoint.sh` script and copies it to the correct location. Then we list the content of the ssh directory.

The next step is to run a ansible command! Most of the time you'll be doing a `ansible-playbook` command. So your base command will be something like this:

`docker run -it --rm -v /c/Users/user..../ssh/:/ansible/ssh/ 111-ansible ansible-playbook`

Just with parameters on the end. If you run this command as is, it will give you all the possible options.

Normally we'll run that above command with the parameter of a YAML file appended, which will just run the playbook.

Some interesting options are:

- --limit=HOST_PATTERN
  - Allows us to run playbooks only on certain hosts.
  - --limit=staging means only run on staging machines.
  - --limit=load-balancer means only run on haproxy servers.
  - More complex combinations can be made, including specific hosts.
- --tag=TAG
  - If we tag our roles and playbooks correctly, it means we can run the entire playbook but skip out the stuff we know we haven't changed
  - For example, only updating Splunk on each host.
- -C
  - Does a dry-run and doesn't make any changes.

## Provisioning

1. In either PowerShell or bash, cd to the git repo you cloned.
2. Run `docker build -t 111-ansible .` to create a docker image which can run ansible.
3. Run

`docker run  -e AZURE_AD_USER=USER.NAME@nhschoices.net
            -e AZURE_PASSWORD=PASSWORD
            -e AZURE_SUBSCRIPTION_ID=SUB_ID 111-ansible
            ansible ansible-playbook provision_production.yml`

This will run the ansible playbook which create the Azure VMs.

## Running Ansible via bastion SSH host

As not all the VMs will be available by SSH over a public IP nor will we have a VPN setup, we cannot run ansible playbooks against them.

The way around this is to proxy SSH through an IP/VM which is internet facing. To do this in each playbook we set the following variable:
`
  vars:
    ansible_ssh_common_args: '-o ProxyCommand="ssh -W %h:%p -q ansible@ip-111-s-jump.northeurope.cloudapp.azure.com"'
`

In this example, we route the SSH commands through the `dn-p-mgmt` public IP using user `ansible`.

# Playbooks

- all.yml
  - Installs docker, docker-compose, etc.

# Roles

- azurefile-dockervolumedriver
  - Installs and configures [Azure Volume Driver](https://github.com/Azure/azurefile-dockervolumedriver)
- choices_backup
  - Creates a cron job that uses docker to backup directories to Azure
- docker
  - Installs and configures docker
- docker_client
  - Configures TLS for the docker client machine
- docker_registry
  - Configures and runs the docker registry
- haproxy
  - Installs HAProxy
- splunk
  - Installs a full Splunk instance as a docker image
- splunk_uf
  - Install the Splunk UF remotely
- users
  - Add and configures users, groups, sudo and their SSH keys
